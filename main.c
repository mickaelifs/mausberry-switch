#include "maus.h"

#include <glib.h>
#include <glib/gprintf.h>
#include <glib-unix.h>
#include <stdlib.h>

gboolean maus_handle_sighup(gpointer user_data)
{
    MausPrivate *priv = (MausPrivate*) user_data;
    g_printf("Received SIGHUP, reloading configuration.\n");
    maus_reload_config(priv);
    maus_setup_gpio(priv);

    return G_SOURCE_CONTINUE;
}

gboolean maus_handle_termint(gpointer user_data)
{
    MausPrivate *priv = (MausPrivate*) user_data;
    g_fprintf(stderr, "Received SIGTERM or SIGINT, exiting.\n");
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[])
{
    int start_success = 0;
    int shutdown_success = 0;
    MausPrivate *priv = g_new0(MausPrivate, 1);
    int state=1;		/* ON/OFF start with OFF*/
    int led_state=1;

    // Set up signal handlers
    //g_unix_signal_add(SIGHUP, (GSourceFunc) maus_handle_sighup, priv);
    //g_unix_signal_add(SIGINT, (GSourceFunc) maus_handle_termint, priv);
    //g_unix_signal_add(SIGINT, (GSourceFunc) maus_handle_termint, priv);

    // Parse config
    if(!maus_reload_config(priv)) {
        g_fprintf(stderr, "Critical: exiting.\n");
        exit(EXIT_FAILURE);
    }

    // start AP at boot
    g_printf("Starting AP.\n");
    start_success = system(priv->startap_command);

    // Set up pins
    maus_setup_gpio(priv);

    // Wait for switch state to change
    int count=0;
    int timer=0;
    int last_result=-1;
    int led_off=0;
    while(1) {
	    int result;
            //printf("Wait on gpio signal\n");
            result = maus_gpio_wait(priv->pin_out);
            //printf("gpio signal received\n");
	    if(count==0) {
		    last_result=result;
	    }
	    if(last_result == result && result == 1 ) {
		    count++;
	    } else {
		    count=0;
	    }
	    if(count==priv->delay) {  
		state=(state+1)%2;
		g_printf("Switching to state = %d\n",state);
		if(state==1) {
		    g_printf("Starting AP.\n");
		    start_success = system(priv->startap_command);
		} else if (state==0) {
		    g_printf("Stopping AP.\n");
                    timer=0;
		    shutdown_success = system(priv->shutdown_command);
		}
            }
    	    //g_printf("Power switch pressed! (received a %d count %d)!\n", result,count);
	    usleep(100000);   // 1/10s
	    /* if state if "ON", make the led flash */
	    if(state==1) {
		    led_state=(led_state+1)%2;
    		    set_led_state(led_state,led_state,led_state);
                    maus_gpio_write(16,led_state);
                    if(timer<10) {
			printf("led is flashing now\n");
                    }
		    timer++;
		    led_off=0;
	    } else {	
    		    set_led_state(0,0,1);
                    /* maus_gpio_write(16,led_state); */
                    maus_gpio_write(16,0);
		    if(led_off < 5) {
			printf("led is turned off\n");
                        led_off++;
		    }
            }
            /* if we reach 4h :
              - do as if we switched the button off 
              - turn wifi off  */

            if(timer > (4*1*60*60*10))    // 1s * 60 * 60 * 1/10s (3h)
            //if(timer > (1*20*10))    // 20s
            {
		    timer=0;
                    g_printf("Stopping AP because of timeout.\n");
		    shutdown_success = system(priv->shutdown_command);
                    state=0;
            }
            
    }
    	

}
