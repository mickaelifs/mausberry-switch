#ifndef __MAUS_H
#define __MAUS_H

#include <glib.h>

typedef struct {
    gchar *shutdown_command;
    gchar *startap_command;
    gint  delay;
    gint  pin_in;
    gint  pin_out;
} MausPrivate;

typedef enum {
    DIRECTION_IN  = 0,
    DIRECTION_OUT = 1
} MausGpioDirection;

typedef enum {
    VALUE_LOW  = 0,
    VALUE_HIGH = 1
} MausGpioValue;

int set_led_state(gint led1_state, gint led2_state, gint led3_state);
int maus_gpio_export(gint pin);
int maus_gpio_unexport(gint pin);
int maus_gpio_direction(gint pin, gint dir);
int maus_gpio_interrupt(gint pin);
int maus_gpio_wait(gint pin);
int maus_gpio_write(gint pin, gint value);
gboolean maus_reload_config(MausPrivate *priv);
gboolean maus_setup_gpio(MausPrivate *priv);

#endif
